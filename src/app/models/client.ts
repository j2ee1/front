import {Compte} from './compte';

export class Client {
  id:number;
  firstName:string;
  lastName:string;
  phone:string;
  adress:string;
  comptes:Compte[];

}
