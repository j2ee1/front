import {Client} from './client';

export class User {
  accessToken:string;
  email:string;
  id:number;
  roles:string[];
  username:string;
  client:Client;
}
