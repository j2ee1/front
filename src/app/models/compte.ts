import {Client} from './client';

export class Compte {
  rib: number;
  solde: number;
  client: Client;
}
