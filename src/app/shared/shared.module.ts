import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from './material.module';
import {HttpClientModule} from '@angular/common/http';
import {TndPipe} from '../pipes/tnd.pipe';
import {FullNamePipe} from '../pipes/full-name.pipe';

@NgModule({
  declarations:[TndPipe,FullNamePipe],
  imports:[
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    TndPipe,
    FullNamePipe
  ]

})
export class SharedModule{}
