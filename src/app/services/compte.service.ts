import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Compte} from '../models/compte';

const API = `${environment.apiUrl}/api/v1/comptes`;

@Injectable({
  providedIn: 'root'
})
export class CompteService {

  constructor(private httpClient: HttpClient) {
  }

  public getAll(): Observable<Compte[]> {
    return this.httpClient.get<Compte[]>(`${API}/all`);
  }

  public delete(rib: number): Observable<any> {
    return this.httpClient.delete(`${API}/${rib}`, {responseType: 'text'});
  }

  public add(compte: any): Observable<any> {
    return this.httpClient.post(`${API}/add`, compte, {responseType: 'text'});
  }

  public get(rib: number): Observable<Compte> {
    return this.httpClient.get<Compte>(`${API}/${rib}`);
  }

  public update(rib: number, compte: any): Observable<any> {
    return this.httpClient.put(`${API}/${rib}`, compte, {responseType: 'text'});
  }
  public getByClientId(id:number):Observable<Compte[]>{
    return this.httpClient.get<Compte[]>(`${API}/client/${id}`);
  }

}
