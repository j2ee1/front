import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Client} from '../models/client';
import {User} from '../models/user';

const API = `${environment.apiUrl}/api/v1/users`;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
  }

  public getAll(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${API}/all`);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete(`${API}/${id}`, {responseType: 'text'});
  }

  public add(client: any): Observable<any> {
    return this.httpClient.post(`${API}/add`, client, {responseType: 'text'});
  }

  public get(id: number): Observable<User> {
    return this.httpClient.get<User>(`${API}/${id}`);
  }

  public update(id: number, user: any): Observable<any> {
    return this.httpClient.put(`${API}/${id}`, user, {responseType: 'text'});
  }
}
