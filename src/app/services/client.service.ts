import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Client} from '../models/client';
const API = `${environment.apiUrl}/api/v1/clients`;
@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private httpClient: HttpClient) {
  }

  public getAll(): Observable<Client[]> {
    return this.httpClient.get<Client[]>(`${API}/all`);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete(`${API}/${id}`, {responseType: 'text'});
  }

  public add(client: any): Observable<any> {
    return this.httpClient.post(`${API}/add`, client, {responseType: 'text'});
  }

  public get(id: number): Observable<Client> {
    return this.httpClient.get<Client>(`${API}/${id}`);
  }

  public update(id: number, client: any): Observable<any> {
    return this.httpClient.put(`${API}/${id}`, client, {responseType: 'text'});
  }

  public getByUserId(id:number):Observable<Client>{
    return this.httpClient.get<Client>(`${API}/user/${id}`);
  }

}
