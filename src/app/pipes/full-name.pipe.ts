import { Pipe, PipeTransform } from '@angular/core';
import {Client} from '../models/client';

@Pipe({
  name: 'fullName'
})
export class FullNamePipe implements PipeTransform {

  transform(value: Client, ...args: unknown[]): unknown {
    if(value==null)
      return "";
    return value.firstName+" "+value.lastName;
  }

}
