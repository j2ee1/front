import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tnd'
})
export class TndPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): unknown {
    return "TND "+value.toString();
  }

}
