import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formGroup:FormGroup;
  constructor(private formBuilder:FormBuilder,private authService:AuthService,private router:Router,private toastService:ToastrService) { }

  ngOnInit(): void {
    this.formGroup=this.formBuilder.group({
      'username':['',Validators.required],
      'password':['',[Validators.required,Validators.minLength(8)]],
    });
  }
  submit()
  {
    if(this.formGroup.valid)
    {
      this.authService.login(this.formGroup.value).subscribe((data)=>{

        if(data.roles.includes(environment.admin))
        {
          this.toastService.success("","Welcome back "+data.username,{
            positionClass:"toast-top-center",
            timeOut:1000,
          });
        }
        this.router.navigate(['welcome']);
      },((error) => {
        this.toastService.error("Wrong Credentials or username doesn't exist","Error",{
          positionClass:"toast-bottom-center",
          closeButton:false,
          timeOut:2000,
        });
      }));
    }
  }

}
