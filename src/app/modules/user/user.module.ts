import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAddComponent } from './user-add/user-add.component';
import { UserListComponent } from './user-list/user-list.component';
import {UserRoutingModule} from './user-routing.module';
import {SharedModule} from '../../shared/shared.module';



@NgModule({
  declarations: [UserAddComponent, UserListComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule
  ]
})
export class UserModule { }
