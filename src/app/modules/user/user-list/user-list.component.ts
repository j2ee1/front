import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Compte} from '../../../models/compte';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {CompteService} from '../../../services/compte.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {UserService} from '../../../services/user.service';
import {User} from '../../../models/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  displayedColumns: string[] = ['id', 'username', 'email', 'client','actions'];
  dataSource: MatTableDataSource<User>;
  public users: User[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private compteService: CompteService, private router: Router,private userService:UserService) {
  }

  ngOnInit() {
    this.userService.getAll().subscribe(
      (data) => {
        this.users = data;
        console.log(data);
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (error) => {
        Swal.fire('Error', error.message, 'error');
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  delete(id: number) {
    Swal.fire({
      title: 'Deleting User ' + id,
      text: 'Are you sure you want to delete this user ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {

        if (result.isConfirmed) {
          this.userService.delete(id).subscribe(
            (data) => {
              this.users = this.users.filter((c) => c.id != id);
              this.dataSource = new MatTableDataSource(this.users);
              Swal.fire(
                'Deleted!',
                'Your user has been successfully deleted.',
                'success'
              );
            }, (error) => {
              Swal.fire('Error', error.error, 'error');
            }
          );
        }
      }
    );
  }


}
