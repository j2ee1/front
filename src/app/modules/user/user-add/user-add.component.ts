import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClientService} from '../../../services/client.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {Client} from '../../../models/client';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {
  hide = true;
  loaded:boolean;
  formGroup: FormGroup;
  clients: Client[] = [];
  filteredClients: Observable<Client[]>;

  constructor(private formBuilder: FormBuilder,private clientService:ClientService,
              private router:Router,private userService:UserService) {
  }

  ngOnInit(): void {
    this.clientService.getAll().subscribe((data) => {
      this.clients = data;
      this.formGroup = this.formBuilder.group({
        username: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        password:['',[Validators.required,Validators.minLength(8)]],
        client:['',Validators.required]
      });
      this.filteredClients = this.formGroup.controls['client'].valueChanges.pipe(
        startWith(''), map(value => value ? this._filter(value) : this.clients.slice()));
      this.loaded = true;
    });

  }

  displayFn(client: Client): string {
    return client ? client.firstName + ' ' + client.lastName : '';
  }

  private _filter(value: string): Client[] {
    if(typeof value==='string'){
      const filterValue = value.toLowerCase();
      return this.clients.filter(client => {
        let fullName = client.firstName.toLowerCase() + ' ' + client.lastName.toLowerCase();
        return fullName.indexOf(filterValue.toLowerCase()) === 0;
      });
    }

  }

  submit() {
    if (this.formGroup.valid) {
      this.userService.add(this.formGroup.value).subscribe(
        (data)=>{
          Swal.fire('User', data, 'success').then((value) => {
            console.log(value);
            if(value.isConfirmed){
              this.router.navigate(['users/list']);
            }
          });

        },(error) => {
          Swal.fire('Error', error.error, 'error');
        }
      )
    }
  }

}
