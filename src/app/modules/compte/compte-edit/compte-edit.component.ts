import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CompteService} from '../../../services/compte.service';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from "sweetalert2";
import {ClientService} from '../../../services/client.service';
import {forkJoin} from 'rxjs';
import {Client} from '../../../models/client';

@Component({
  selector: 'app-compte-edit',
  templateUrl: './compte-edit.component.html',
  styleUrls: ['./compte-edit.component.scss']
})
export class CompteEditComponent implements OnInit {

  loaded:boolean=false;
  accountExist:boolean=true;
  formGroup: FormGroup;
  clients:Client[]=[];
  client:Client;
  rib:number;
  constructor(
    private formBuilder: FormBuilder,
    private compteService:CompteService,
    private router:Router,
    private route:ActivatedRoute,
    private clientService:ClientService
    ) {
  }

  ngOnInit(): void {
    this.rib=this.route.snapshot.params.rib;
    let compteApi= this.compteService.get(this.rib);
    let clientApi=this.clientService.getAll();
    forkJoin([compteApi,clientApi]).subscribe((results)=>{
      this.clients=results[1];
      this.client=results[0].client;
      this.accountExist=true;
      this.formGroup = this.formBuilder.group({
        solde: [results[0].solde, [Validators.required, Validators.pattern('([0-9]*[.])?[0-9]*')]],
      });
      this.loaded=true;
    },(error) => {
      this.loaded=true;
      this.accountExist=false;
      console.log(error);
      Swal.fire('Error', error.error, 'error').then(value => {
        if(value.isConfirmed){
          this.router.navigate(['comptes/list']);
        }
      });
    })
  }

  submit() {
    if (this.formGroup.valid) {
      // let data={'solde':this.formGroup.value.solde,'client':this.client};
      this.compteService.update(this.rib,this.formGroup.value).subscribe(
        (data)=>{
          Swal.fire('Account', data, 'success').then((value) => {
            console.log(value);
            if(value.isConfirmed){
              this.router.navigate(['comptes/list']);
            }
          });

        },(error) => {
          Swal.fire('Error', error.error, 'error');
        }
      )
    }
  }

}
