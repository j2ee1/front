import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompteListComponent } from './compte-list/compte-list.component';
import { CompteAddComponent } from './compte-add/compte-add.component';
import {CompteRoutingModule} from './compte-routing.module';
import {SharedModule} from '../../shared/shared.module';
import { CompteEditComponent } from './compte-edit/compte-edit.component';



@NgModule({
  declarations: [CompteListComponent, CompteAddComponent, CompteEditComponent],
  imports: [
    CommonModule,
    CompteRoutingModule,
    SharedModule
  ]
})
export class CompteModule { }
