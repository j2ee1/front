import {Component, OnInit, ViewChild} from '@angular/core';
import Swal from 'sweetalert2';
import {MatTableDataSource} from '@angular/material/table';
import {Compte} from '../../../models/compte';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {CompteService} from '../../../services/compte.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-compte-list',
  templateUrl: './compte-list.component.html',
  styleUrls: ['./compte-list.component.scss']
})
export class CompteListComponent implements OnInit {
  displayedColumns: string[] = ['rib', 'solde', 'client', 'actions'];
  dataSource: MatTableDataSource<Compte>;
  public comptes: Compte[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private compteService: CompteService, private router: Router) {
  }

  ngOnInit() {
    this.compteService.getAll().subscribe(
      (results) => {
        this.comptes = results;
        this.dataSource = new MatTableDataSource(this.comptes);
        this.dataSource.filterPredicate=(data,filter)=>{
          let fullNameClient= data.client.firstName+" "+data.client.lastName;
            return data.solde.toString().indexOf(filter)!==-1
          || data.rib.toString().indexOf(filter)!==-1
          || fullNameClient.trim().toLowerCase().indexOf(filter)!==-1;
        };
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (error) => {
        Swal.fire('Error', error.message, 'error');
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;

    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  edit(rib: number) {
    console.log(rib);
    this.router.navigate(['/comptes/edit', rib]);
  }


  delete(rib: number) {
    Swal.fire({
      title: 'Deleting Account ' + rib,
      text: 'Are you sure you want to delete this account ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {

        if (result.isConfirmed) {
          this.compteService.delete(rib).subscribe(
            (data) => {
              this.comptes = this.comptes.filter((c) => c.rib != rib);
              this.dataSource = new MatTableDataSource(this.comptes);
              Swal.fire(
                'Deleted!',
                'Your account has been successfully deleted.',
                'success'
              );
            }, (error) => {
              Swal.fire('Error', error.error, 'error');
            }
          );
        }
      }
    );
  }

}
