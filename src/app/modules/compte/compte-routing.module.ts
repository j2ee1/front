import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CompteListComponent} from './compte-list/compte-list.component';
import {CompteAddComponent} from './compte-add/compte-add.component';
import {CompteEditComponent} from './compte-edit/compte-edit.component';

const routes: Routes = [
  {path: 'list', component: CompteListComponent},
  {path:'add',component:CompteAddComponent},
  {path:'edit/:rib',component:CompteEditComponent},
  {path:'',redirectTo:'list',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompteRoutingModule {
}
