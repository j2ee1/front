import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Compte} from '../../../models/compte';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {CompteService} from '../../../services/compte.service';
import {Router} from '@angular/router';
import Swal from "sweetalert2";
import {Client} from '../../../models/client';
import {ClientService} from '../../../services/client.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {
  displayedColumns: string[] = ['id','firstName', 'lastName', 'phone','adress', 'actions'];
  dataSource: MatTableDataSource<Client>;
  public clients: Client[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private clientService: ClientService, private router: Router) {
  }

  ngOnInit() {
    this.clientService.getAll().subscribe(
      (data) => {
        this.clients = data;
        console.log(data);
        this.dataSource = new MatTableDataSource(this.clients);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (error) => {
        Swal.fire('Error', error.message, 'error');
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  edit(id: number) {
    this.router.navigate(['/clients/edit', id]);
  }
  view(id:number){
    this.router.navigate(['/clients/view',id]);
  }

  delete(id: number) {
    Swal.fire({
      title: 'Deleting Client ' + id,
      text: 'Are you sure you want to delete this client ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {

        if (result.isConfirmed) {
          this.clientService.delete(id).subscribe(
            (data) => {
              this.clients=this.clients.filter((c)=>c.id!=id);
              this.dataSource = new MatTableDataSource(this.clients);
              Swal.fire(
                'Deleted!',
                'Your client has been successfully deleted.',
                'success'
              );
            }, (error) => {
              Swal.fire("Error",error.error,"error");
            }
          );
        }
      }
    );
  }
}
