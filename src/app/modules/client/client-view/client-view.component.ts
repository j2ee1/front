import { Component, OnInit } from '@angular/core';
import {ClientService} from '../../../services/client.service';
import {Client} from '../../../models/client';
import {ActivatedRoute} from '@angular/router';
import {CompteService} from '../../../services/compte.service';
import {forkJoin} from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-client-view',
  templateUrl: './client-view.component.html',
  styleUrls: ['./client-view.component.scss']
})
export class ClientViewComponent implements OnInit {

  loaded:boolean;
  client:Client;
  constructor(
    private route:ActivatedRoute,
    private clientService:ClientService,
    private compteService:CompteService,
  ) { }

  ngOnInit(): void {
    let id=this.route.snapshot.params.id;
    let clientApi=this.clientService.get(id);
    let compteApi=this.compteService.getByClientId(id);
    forkJoin([clientApi,compteApi]).subscribe((results)=>{
      this.client=results[0];
      this.client.comptes=results[1];
      this.loaded=true;
    },(error)=>{
      Swal.fire("Error",error.error,"error");
    })
  }

}
