import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CompteService} from '../../../services/compte.service';
import {Router} from '@angular/router';
import Swal from "sweetalert2";
import {ClientService} from '../../../services/client.service';

@Component({
  selector: 'app-client-add',
  templateUrl: './client-add.component.html',
  styleUrls: ['./client-add.component.scss']
})
export class ClientAddComponent implements OnInit {

  formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,private clientService:ClientService,private router:Router) {
  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern('[a-zA-Z]*')]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z]*')]],
      phone:['',[Validators.required,Validators.pattern('[0-9]*'),Validators.minLength(8),Validators.maxLength(8)]],
      adress:['',Validators.required]
    });
  }

  submit() {
    if (this.formGroup.valid) {
      this.clientService.add(this.formGroup.value).subscribe(
        (data)=>{
          Swal.fire('Client', data, 'success').then((value) => {
            console.log(value);
            if(value.isConfirmed){
              this.router.navigate(['clients/list']);
            }
          });

        },(error) => {
          Swal.fire('Error', error.error, 'error');
        }
      )
    }
  }

}
