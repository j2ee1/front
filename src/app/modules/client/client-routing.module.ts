import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ClientListComponent} from './client-list/client-list.component';
import {ClientAddComponent} from './client-add/client-add.component';
import {ClientEditComponent} from './client-edit/client-edit.component';
import {ClientViewComponent} from './client-view/client-view.component';

const routes: Routes = [
  {path: 'list', component: ClientListComponent},
  {path:'add',component:ClientAddComponent},
  {path:'edit/:id',component:ClientEditComponent},
  {path:'view/:id',component:ClientViewComponent},
  {path:'',redirectTo:'list',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule {
}
