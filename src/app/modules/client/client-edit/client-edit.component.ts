import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CompteService} from '../../../services/compte.service';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from "sweetalert2";
import {ClientService} from '../../../services/client.service';

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.scss']
})
export class ClientEditComponent implements OnInit {
  loaded:boolean=false;
  clientExist:boolean=true;
  formGroup: FormGroup;
  id:number;
  constructor(private formBuilder: FormBuilder,private clientService:ClientService,private router:Router,private route:ActivatedRoute) {
  }

  ngOnInit(): void {
    this.id=this.route.snapshot.params.id;
    this.clientService.get(this.id).subscribe((data)=>{
        this.clientExist=true;
        this.formGroup = this.formBuilder.group({
          firstName: [data.firstName, [Validators.required, Validators.pattern('[a-zA-Z]*')]],
          lastName: [data.lastName, [Validators.required, Validators.pattern('[a-zA-Z]*')]],
          phone:[data.phone,[Validators.required,Validators.pattern('[0-9]*'),Validators.minLength(8),Validators.maxLength(8)]],
          adress:[data.adress,Validators.required]
        });
      this.loaded=true;
    },(error) => {
      this.loaded=true;
      this.clientExist=false;
      console.log(error);
      Swal.fire('Error', error.error, 'error').then(value => {
        if(value.isConfirmed){
          this.router.navigate(['comptes/list']);
        }
      });
    })
  }

  submit() {
    if (this.formGroup.valid) {
      this.clientService.update(this.id,this.formGroup.value).subscribe(
        (data)=>{
          Swal.fire('Client', data, 'success').then((value) => {
            console.log(value);
            if(value.isConfirmed){
              this.router.navigate(['clients/list']);
            }
          });

        },(error) => {
          Swal.fire('Error', error.error, 'error');
        }
      )
    }
  }

}
