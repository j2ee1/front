import { Component, OnInit } from '@angular/core';
import {ClientService} from '../../services/client.service';
import {CompteService} from '../../services/compte.service';
import {TokenStorageService} from '../../services/token-storage.service';
import {Client} from '../../models/client';
import {Compte} from '../../models/compte';
import {User} from '../../models/user';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  loaded=false;
  user:User;
  client:Client;
  comptes:Compte[]=[];
  constructor(private clientService:ClientService,private compteService:CompteService,
              private tokenStorageService:TokenStorageService) { }

              //TODO: Add Accordion Angular Material for each account
  ngOnInit(): void {
    this.user=this.tokenStorageService.getUser();
    let id=this.user.id;
    this.clientService.getByUserId(id).subscribe((data)=>{
      console.log(data);
      this.client=data;
      this.compteService.getByClientId(this.client.id).subscribe((comptes)=>{
        this.comptes=comptes;
        console.log(comptes);
        this.loaded=true;
      })
    })
  }

}
