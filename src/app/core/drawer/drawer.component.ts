import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss']
})
export class DrawerComponent implements OnInit {

  @Input()
  @HostBinding('class.drawer-open')
  isDrawerOpen: boolean;

  @Input()
  isAdmin:boolean;

  @Output()
  drawerToggleEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private authService:AuthService) {
  }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
  }
}
