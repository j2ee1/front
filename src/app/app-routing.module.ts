import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from './guards/auth.guard';
import {AnonymousGuard} from './guards/anonymous.guard';
import {WelcomeComponent} from './core/welcome/welcome.component';
import {UserGuard} from './guards/user.guard';
import {AdminGuard} from './guards/admin.guard';

const routes: Routes = [
  {path: '', redirectTo: 'comptes', pathMatch: 'full'},
  {path: 'comptes', loadChildren: () => import('./modules/compte/compte.module').then(m => m.CompteModule),canActivate:[AuthGuard,AdminGuard]},
  {path: 'clients', loadChildren: () => import('./modules/client/client.module').then(m => m.ClientModule),canActivate:[AuthGuard,AdminGuard]},
  {path: 'users', loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule),canActivate:[AuthGuard,AdminGuard]},
  {path: 'auth', loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),canActivate:[AnonymousGuard]},
  {path:'welcome',component:WelcomeComponent,canActivate:[AuthGuard,UserGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
