import {Component, HostBinding, OnInit} from '@angular/core';
import {AuthService} from './services/auth.service';
import {TokenStorageService} from './services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  @HostBinding("class.drawer-open")
  isDrawerOpen: boolean = false;

  toggleDrawer(isDrawerOpen: boolean) {
    this.isDrawerOpen = isDrawerOpen;
  }

  isLoggedIn;
  isAdmin;
  constructor(private authService:AuthService,private tokenStorageService:TokenStorageService) {
  }
  ngOnInit() {
    this.authService.currentUser.subscribe((data)=>{
      this.isLoggedIn=this.tokenStorageService.isLoggedIn();
      this.isAdmin=this.tokenStorageService.isAdmin();
    })
  }

}
